package application.controler;

import application.Main;
import application.model.TypeC;
import application.model.TypeFoncPosInit;
import application.model.TypeFoncVitInit;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ListenerTask extends Task<Void>{
	
	private Main main;
	
	public ListenerTask(Main main) {
		this.main=main;
	}

	@Override
	protected Void call() throws Exception {
		main.getEquation().getTemps().addListener((observable, oldValue, newValue)->{
			Platform.runLater(()->{
				main.getTempsGridPaneMapping().getTempsValue().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			});
		});
		main.getLayoutChartGridPaneMapping().getxLowChartTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getLineChart().getxAxis().setLowerBound(Double.parseDouble(newValue));
				main.getLineChart().getxAxis().setTickUnit((main.getLineChart().getxAxis().getUpperBound()-main.getLineChart().getxAxis().getLowerBound())/10);
			}catch(NumberFormatException e) {}
		});
		main.getLayoutChartGridPaneMapping().getxHighChartTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getLineChart().getxAxis().setUpperBound(Double.parseDouble(newValue));
				main.getLineChart().getxAxis().setTickUnit((main.getLineChart().getxAxis().getUpperBound()-main.getLineChart().getxAxis().getLowerBound())/10);
			}catch(NumberFormatException e) {}
		});
		main.getLayoutChartGridPaneMapping().getyLowChartTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getLineChart().getyAxis().setLowerBound(Double.parseDouble(newValue));
				main.getLineChart().getyAxis().setTickUnit((main.getLineChart().getyAxis().getUpperBound()-main.getLineChart().getyAxis().getLowerBound())/10);
			}catch(NumberFormatException e) {}
		});
		main.getLayoutChartGridPaneMapping().getyHighChartTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getLineChart().getyAxis().setUpperBound(Double.parseDouble(newValue));
				main.getLineChart().getyAxis().setTickUnit((main.getLineChart().getyAxis().getUpperBound()-main.getLineChart().getyAxis().getLowerBound())/10);
			}catch(NumberFormatException e) {}
		});
		main.getParamFoncGridPaneMapping().getlSlider().valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				double value=Math.round(newValue.doubleValue()*1000)/1000.0;
				main.getEquation().gethBorder().set(value);
				main.getParamFoncGridPaneMapping().getlValue().setText(String.valueOf(value));
				main.getParamFoncGridPaneMapping().getlTextField().setText(String.valueOf(value));
				main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().setMax(value-0.01);
				main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().setMax(value*0.01);
				main.getParabolePosInitGridPaneMapping().gethSlider().setMax(value*0.01);
				main.getPointVitInitGridPaneMapping().getpSlider().setMax(value-0.01);
				main.getDataSeriesControler().calculDatas();
				main.getLineChart().getData().setAll(main.getEquation().getSeries());
			}
		});
		main.getParamFoncGridPaneMapping().getlTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getEquation().gethBorder().set(Double.parseDouble(newValue));
			}catch(NumberFormatException e) {}
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getParamFoncGridPaneMapping().getPosInitComboBox().valueProperty().addListener(new ChangeListener<TypeFoncPosInit>() {

			@Override
			public void changed(ObservableValue<? extends TypeFoncPosInit> observable, TypeFoncPosInit oldValue,
					TypeFoncPosInit newValue) {
				main.getDataSeriesControler().setTypeFoncPosInit(newValue);
				main.getDataSeriesControler().calculDatas();
				switch (oldValue) {
				case CORDE_GUITARE:
					main.gethBox().getChildren().remove(main.getCordeGuitarePosInitGridPaneFXML());
					break;
				case PARABOLE:
					main.gethBox().getChildren().remove(main.getParabolePosInitGridPaneFXML());
				default:
					break;
				}
				switch (newValue) {
				case CORDE_GUITARE:
					main.gethBox().getChildren().add(1, main.getCordeGuitarePosInitGridPaneFXML());
					break;
				case PARABOLE:
					main.gethBox().getChildren().add(1, main.getParabolePosInitGridPaneFXML());
					break;
				default:
					break;
				}
				main.getLineChart().getData().setAll(main.getEquation().getSeries());
			}
		});
		main.getParamFoncGridPaneMapping().getVitInitComboBox().valueProperty().addListener(new ChangeListener<TypeFoncVitInit>() {

			@Override
			public void changed(ObservableValue<? extends TypeFoncVitInit> observable, TypeFoncVitInit oldValue,
					TypeFoncVitInit newValue) {
				main.getDataSeriesControler().setTypeFoncVitInit(newValue);
				main.getDataSeriesControler().calculDatas();
				switch (oldValue) {
				case CONSTANTE:
					main.gethBox().getChildren().remove(main.getConstanteVitInitGridPaneFXML());
					break;
				case UN_POINT:
					main.gethBox().getChildren().remove(main.getPointVitInitGridPaneFXML());
					break;
				default:
					break;
				}
				switch (newValue) {
				case CONSTANTE:
					main.gethBox().getChildren().add(2, main.getConstanteVitInitGridPaneFXML());
					break;
				case UN_POINT:
					main.gethBox().getChildren().add(2, main.getPointVitInitGridPaneFXML());
				default:
					break;
				}
				main.getLineChart().getData().setAll(main.getEquation().getSeries());
			}
		});
		main.getParamFoncGridPaneMapping().getcComboBox().valueProperty().addListener(new ChangeListener<TypeC>() {

			@Override
			public void changed(ObservableValue<? extends TypeC> observable, TypeC oldValue, TypeC newValue) {
				main.getDataSeriesControler().setTypeC(newValue);
				main.getDataSeriesControler().calculDatas();
				switch (oldValue) {
				case CONSTANTE:
					main.gethBox().getChildren().remove(main.getConstanteCCarreeGridPaneFXML());
					break;
				case AFFINE:
					main.gethBox().getChildren().remove(main.getAffineCCarreeGridPaneFXML());
					break;
				default:
					break;
				}
				switch (newValue) {
				case CONSTANTE:
					main.gethBox().getChildren().add(0, main.getConstanteCCarreeGridPaneFXML());
					break;
				case AFFINE:
					main.gethBox().getChildren().add(0, main.getAffineCCarreeGridPaneFXML());
				default:
					break;
				}
				main.getLineChart().getData().setAll(main.getEquation().getSeries());
			}
		});
		main.getButtonGridPaneMapping().getDemarrerButton().setOnAction((e)->{
			if(1>=main.getEquation().getcValues().stream().mapToDouble((a)->a.doubleValue()).max().orElse(1)*main.getEquation().getDx().doubleValue()/main.getEquation().getDt().doubleValue()) {
				main.getAffineCCarreeGridPaneMapping().getbSlider().disableProperty().set(true);
				main.getAffineCCarreeGridPaneMapping().getbTextField().disableProperty().set(true);
				main.getAffineCCarreeGridPaneMapping().getmSlider().disableProperty().set(true);
				main.getAffineCCarreeGridPaneMapping().getmTextField().disableProperty().set(true);
				main.getConstanteCCarreeGridPaneMapping().getcSlider().disableProperty().set(true);
				main.getConstanteCCarreeGridPaneMapping().getcTextField().disableProperty().set(true);
				main.getButtonGridPaneMapping().getDemarrerButton().disableProperty().set(true);
				main.getButtonGridPaneMapping().getArreterButton().disableProperty().set(false);
				main.getButtonGridPaneMapping().getRemiseZeroButton().disableProperty().set(true);
				main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().disableProperty().set(true);
				main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().disableProperty().set(true);
				main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().disableProperty().set(true);
				main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().disableProperty().set(true);
				main.getParabolePosInitGridPaneMapping().gethSlider().disableProperty().set(true);
				main.getParabolePosInitGridPaneMapping().gethTextField().disableProperty().set(true);
				main.getConstanteVitInitGridPaneMapping().getvSlider().disableProperty().set(true);
				main.getConstanteVitInitGridPaneMapping().getvTextField().disableProperty().set(true);
				main.getParamFoncGridPaneMapping().getlSlider().disableProperty().set(true);
				main.getParamFoncGridPaneMapping().getlTextField().disableProperty().set(true);
				main.getParamFoncGridPaneMapping().getPosInitComboBox().disableProperty().set(true);
				main.getParamFoncGridPaneMapping().getVitInitComboBox().disableProperty().set(true);
				main.getParamFoncGridPaneMapping().getcComboBox().disableProperty().set(true);
				main.getPointVitInitGridPaneMapping().getvSlider().disableProperty().set(true);
				main.getPointVitInitGridPaneMapping().getvTextField().disableProperty().set(true);
				main.getPointVitInitGridPaneMapping().getpSlider().disableProperty().set(true);
				main.getPointVitInitGridPaneMapping().getpTextField().disableProperty().set(true);
				main.getPersonGridPaneMapping().getDtTextField().disableProperty().set(true);
				main.getPersonGridPaneMapping().getDxTextField().disableProperty().set(true);
				main.getDataSeriesControler().calculDatas();
				main.getTimeControler().startTime();
			}else {
				Alert alert=new Alert(AlertType.WARNING);
				alert.setTitle("Attention");
				alert.setHeaderText("Attention! Les donn�es entr�es rendent le syst�me instable.");
				alert.showAndWait();
			}
		});
		main.getButtonGridPaneMapping().getArreterButton().setOnAction((e)->{
			main.getButtonGridPaneMapping().getDemarrerButton().disableProperty().set(false);
			main.getButtonGridPaneMapping().getArreterButton().disableProperty().set(true);
			main.getButtonGridPaneMapping().getRemiseZeroButton().disableProperty().set(false);
			main.getTimeControler().stopTime();
		});
		main.getButtonGridPaneMapping().getRemiseZeroButton().setOnAction((e)->{
			main.getAffineCCarreeGridPaneMapping().getbSlider().setValue(0);
			main.getAffineCCarreeGridPaneMapping().getbSlider().disableProperty().set(false);
			main.getAffineCCarreeGridPaneMapping().getbTextField().setText(String.valueOf(0));
			main.getAffineCCarreeGridPaneMapping().getbTextField().disableProperty().set(false);
			main.getAffineCCarreeGridPaneMapping().getmSlider().setValue(0.5);
			main.getAffineCCarreeGridPaneMapping().getmSlider().disableProperty().set(false);
			main.getAffineCCarreeGridPaneMapping().getmTextField().setText(String.valueOf(0.5));
			main.getAffineCCarreeGridPaneMapping().getmTextField().disableProperty().set(false);
			main.getConstanteCCarreeGridPaneMapping().getcSlider().setValue(0.5);
			main.getConstanteCCarreeGridPaneMapping().getcSlider().disableProperty().set(false);
			main.getConstanteCCarreeGridPaneMapping().getcTextField().setText(String.valueOf(0.5));
			main.getConstanteCCarreeGridPaneMapping().getcTextField().disableProperty().set(false);
			main.getButtonGridPaneMapping().getRemiseZeroButton().disableProperty().set(true);
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().setValue(0.0025);
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().disableProperty().set(false);
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().setText(String.valueOf(0.0025));
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().disableProperty().set(false);
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().setValue(0.25);
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().disableProperty().set(false);
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().setText(String.valueOf(0.25));
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().disableProperty().set(false);
			main.getParabolePosInitGridPaneMapping().gethSlider().setValue(0.0025);
			main.getParabolePosInitGridPaneMapping().gethSlider().disableProperty().set(false);
			main.getParabolePosInitGridPaneMapping().gethTextField().setText(String.valueOf(0.0025));
			main.getParabolePosInitGridPaneMapping().gethTextField().disableProperty().set(false);
			main.getConstanteVitInitGridPaneMapping().getvSlider().setValue(0);
			main.getConstanteVitInitGridPaneMapping().getvSlider().disableProperty().set(false);
			main.getConstanteVitInitGridPaneMapping().getvTextField().setText(String.valueOf(0));
			main.getConstanteVitInitGridPaneMapping().getvTextField().disableProperty().set(false);
			main.getLayoutChartGridPaneMapping().getxLowChartTextField().setText(String.valueOf(0));
			main.getLayoutChartGridPaneMapping().getxHighChartTextField().setText(String.valueOf(0.5));
			main.getLayoutChartGridPaneMapping().getyLowChartTextField().setText(String.valueOf(-0.01));
			main.getLayoutChartGridPaneMapping().getyHighChartTextField().setText(String.valueOf(0.01));
			main.getParamFoncGridPaneMapping().getcComboBox().getSelectionModel().select(1);
			main.getParamFoncGridPaneMapping().getcComboBox().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getlSlider().setValue(0.5);
			main.getParamFoncGridPaneMapping().getlSlider().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getlTextField().setText(String.valueOf(0.5));
			main.getParamFoncGridPaneMapping().getlTextField().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getPosInitComboBox().getSelectionModel().selectFirst();
			main.getParamFoncGridPaneMapping().getPosInitComboBox().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getVitInitComboBox().getSelectionModel().selectFirst();
			main.getParamFoncGridPaneMapping().getVitInitComboBox().disableProperty().set(false);
			main.getTempsGridPaneMapping().getTempsValue().setText(String.valueOf(0.0));
			main.getEquation().getTemps().set(0);
			main.getPointVitInitGridPaneMapping().getpSlider().setValue(0.25);
			main.getPointVitInitGridPaneMapping().getpSlider().disableProperty().set(false);
			main.getPointVitInitGridPaneMapping().getpTextField().setText(String.valueOf(0.25));
			main.getPointVitInitGridPaneMapping().getpTextField().disableProperty().set(false);
			main.getPointVitInitGridPaneMapping().getvSlider().setValue(0);
			main.getPointVitInitGridPaneMapping().getvSlider().disableProperty().set(false);
			main.getPointVitInitGridPaneMapping().getvTextField().setText(String.valueOf(0));
			main.getPointVitInitGridPaneMapping().getvTextField().disableProperty().set(false);
			main.getEquation().getDt().set(0.002);
			main.getEquation().getDx().set(0.001);
			main.getAffineCCarreeGridPaneMapping().changeNormal();
			main.getConstanteCCarreeGridPaneMapping().changeNormal();
			main.getConstanteVitInitGridPaneMapping().changeNormal();
			main.getCordeGuitarePosInitGridPaneMapping().changeNormal();
			main.getParabolePosInitGridPaneMapping().changeNormal();
			main.getParamFoncGridPaneMapping().changeNormal();
			main.getPersonGridPaneMapping().changeNormal();
			main.getPointVitInitGridPaneMapping().changeNormal();
			main.getPersonGridPaneMapping().getNormalToggleButton().setSelected(true);
			main.getPersonGridPaneMapping().getDtTextField().disableProperty().set(false);
			main.getPersonGridPaneMapping().getDxTextField().disableProperty().set(false);
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
			main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getGraphicsContext2D().clearRect(0, 0, main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getWidth(), main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getHeight());
		});
		main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurValue().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().textProperty().addListener((a,b,newValue)->{
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionValue().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().textProperty().addListener((a,b,c)->{
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getAffineCCarreeGridPaneMapping().getbSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getAffineCCarreeGridPaneMapping().getbValue().setText(String.valueOf(Math.round(newValue.doubleValue()*100)/100.0));
			main.getAffineCCarreeGridPaneMapping().getbTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
		});
		main.getAffineCCarreeGridPaneMapping().getmSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getAffineCCarreeGridPaneMapping().getmValue().setText(String.valueOf(Math.round(newValue.doubleValue()*100)/100.0));
			main.getAffineCCarreeGridPaneMapping().getmTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
		});
		main.getConstanteCCarreeGridPaneMapping().getcSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getConstanteCCarreeGridPaneMapping().getcValue().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getConstanteCCarreeGridPaneMapping().getcTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
		});
		main.getParabolePosInitGridPaneMapping().gethSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getParabolePosInitGridPaneMapping().gethValue().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getParabolePosInitGridPaneMapping().gethTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getParabolePosInitGridPaneMapping().gethTextField().textProperty().addListener((a,b,c)->{
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
		});
		main.getConstanteVitInitGridPaneMapping().getvSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getConstanteVitInitGridPaneMapping().getvValue().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
			main.getConstanteVitInitGridPaneMapping().getvTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
		});
		main.getPointVitInitGridPaneMapping().getpSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getPointVitInitGridPaneMapping().getpValue().setText(String.valueOf(Math.round(newValue.doubleValue()*100)/100.0));
			main.getPointVitInitGridPaneMapping().getpTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
		});
		main.getPointVitInitGridPaneMapping().getvSlider().valueProperty().addListener((observable, oldValue, newValue)->{
			main.getPointVitInitGridPaneMapping().getvValue().setText(String.valueOf(Math.round(newValue.doubleValue()*100)/100.0));
			main.getPointVitInitGridPaneMapping().getvTextField().setText(String.valueOf(Math.round(newValue.doubleValue()*1000)/1000.0));
		});
		main.getPersonGridPaneMapping().getDtTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getEquation().getDt().set(Double.parseDouble(newValue));
			}catch(NumberFormatException e) {}
		});
		main.getPersonGridPaneMapping().getDxTextField().textProperty().addListener((observable, oldValue, newValue)->{
			try {
				main.getEquation().getDx().set(Double.parseDouble(newValue));
			}catch(NumberFormatException e) {}
		});
		
		main.getPersonGridPaneMapping().getNormalToggleButton().setOnAction((e)->{
			main.getAffineCCarreeGridPaneMapping().getbSlider().setValue(0);
			main.getAffineCCarreeGridPaneMapping().getbSlider().disableProperty().set(false);
			main.getAffineCCarreeGridPaneMapping().getbTextField().setText(String.valueOf(0));
			main.getAffineCCarreeGridPaneMapping().getbTextField().disableProperty().set(false);
			main.getAffineCCarreeGridPaneMapping().getmSlider().setValue(0.5);
			main.getAffineCCarreeGridPaneMapping().getmSlider().disableProperty().set(false);
			main.getAffineCCarreeGridPaneMapping().getmTextField().setText(String.valueOf(0.5));
			main.getAffineCCarreeGridPaneMapping().getmTextField().disableProperty().set(false);
			main.getConstanteCCarreeGridPaneMapping().getcSlider().setValue(0.5);
			main.getConstanteCCarreeGridPaneMapping().getcSlider().disableProperty().set(false);
			main.getConstanteCCarreeGridPaneMapping().getcTextField().setText(String.valueOf(0.5));
			main.getConstanteCCarreeGridPaneMapping().getcTextField().disableProperty().set(false);
			main.getButtonGridPaneMapping().getRemiseZeroButton().disableProperty().set(true);
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().setValue(0.0025);
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().disableProperty().set(false);
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().setText(String.valueOf(0.0025));
			main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().disableProperty().set(false);
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().setValue(0.25);
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().disableProperty().set(false);
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().setText(String.valueOf(0.25));
			main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().disableProperty().set(false);
			main.getParabolePosInitGridPaneMapping().gethSlider().setValue(0.0025);
			main.getParabolePosInitGridPaneMapping().gethSlider().disableProperty().set(false);
			main.getParabolePosInitGridPaneMapping().gethTextField().setText(String.valueOf(0.0025));
			main.getParabolePosInitGridPaneMapping().gethTextField().disableProperty().set(false);
			main.getConstanteVitInitGridPaneMapping().getvSlider().setValue(0);
			main.getConstanteVitInitGridPaneMapping().getvSlider().disableProperty().set(false);
			main.getConstanteVitInitGridPaneMapping().getvTextField().setText(String.valueOf(0));
			main.getConstanteVitInitGridPaneMapping().getvTextField().disableProperty().set(false);
			main.getLayoutChartGridPaneMapping().getxLowChartTextField().setText(String.valueOf(0));
			main.getLayoutChartGridPaneMapping().getxHighChartTextField().setText(String.valueOf(0.5));
			main.getLayoutChartGridPaneMapping().getyLowChartTextField().setText(String.valueOf(-0.01));
			main.getLayoutChartGridPaneMapping().getyHighChartTextField().setText(String.valueOf(0.01));
			main.getParamFoncGridPaneMapping().getcComboBox().getSelectionModel().select(1);
			main.getParamFoncGridPaneMapping().getcComboBox().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getlSlider().setValue(0.5);
			main.getParamFoncGridPaneMapping().getlSlider().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getlTextField().setText(String.valueOf(0.5));
			main.getParamFoncGridPaneMapping().getlTextField().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getPosInitComboBox().getSelectionModel().selectFirst();
			main.getParamFoncGridPaneMapping().getPosInitComboBox().disableProperty().set(false);
			main.getParamFoncGridPaneMapping().getVitInitComboBox().getSelectionModel().selectFirst();
			main.getParamFoncGridPaneMapping().getVitInitComboBox().disableProperty().set(false);
			main.getTempsGridPaneMapping().getTempsValue().setText(String.valueOf(0.0));
			main.getEquation().getTemps().set(0);
			main.getPointVitInitGridPaneMapping().getpSlider().setValue(0.25);
			main.getPointVitInitGridPaneMapping().getpSlider().disableProperty().set(false);
			main.getPointVitInitGridPaneMapping().getpTextField().setText(String.valueOf(0.25));
			main.getPointVitInitGridPaneMapping().getpTextField().disableProperty().set(false);
			main.getPointVitInitGridPaneMapping().getvSlider().setValue(0);
			main.getPointVitInitGridPaneMapping().getvSlider().disableProperty().set(false);
			main.getPointVitInitGridPaneMapping().getvTextField().setText(String.valueOf(0));
			main.getPointVitInitGridPaneMapping().getvTextField().disableProperty().set(false);
			main.getEquation().getDt().set(0.002);
			main.getEquation().getDx().set(0.001);
			main.getAffineCCarreeGridPaneMapping().changeNormal();
			main.getConstanteCCarreeGridPaneMapping().changeNormal();
			main.getConstanteVitInitGridPaneMapping().changeNormal();
			main.getCordeGuitarePosInitGridPaneMapping().changeNormal();
			main.getParabolePosInitGridPaneMapping().changeNormal();
			main.getParamFoncGridPaneMapping().changeNormal();
			main.getPersonGridPaneMapping().changeNormal();
			main.getPointVitInitGridPaneMapping().changeNormal();
			main.getPersonGridPaneMapping().getNormalToggleButton().setSelected(true);
			main.getPersonGridPaneMapping().getDtTextField().disableProperty().set(false);
			main.getPersonGridPaneMapping().getDxTextField().disableProperty().set(false);
			main.getDataSeriesControler().calculDatas();
			main.getLineChart().getData().setAll(main.getEquation().getSeries());
			main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getGraphicsContext2D().clearRect(0, 0, main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getWidth(), main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getHeight());
		});
		main.getPersonGridPaneMapping().getPersonnaliseToggleButton().setOnAction((e)->{
			main.getAffineCCarreeGridPaneMapping().changePerso();
			main.getConstanteCCarreeGridPaneMapping().changePerso();
			main.getConstanteVitInitGridPaneMapping().changePerso();
			main.getCordeGuitarePosInitGridPaneMapping().changePerso();
			main.getParabolePosInitGridPaneMapping().changePerso();
			main.getParamFoncGridPaneMapping().changePerso();
			main.getPersonGridPaneMapping().changePerso();
			main.getPointVitInitGridPaneMapping().changePerso();
		});
		return null;
	}
	
	

}
