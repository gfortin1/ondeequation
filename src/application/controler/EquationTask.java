package application.controler;

import application.Main;
import application.model.Equation;
import javafx.concurrent.Task;

public class EquationTask extends Task<Void>{
	
	private Main main;
	
	public EquationTask(Main main) {
		this.main=main;
	}

	@Override
	protected Void call() throws Exception {
		main.getDataSeriesControler().calculDatas();
		main.getLineChart().getData().addAll(main.getEquation().getSeries());
		return null;
	}

}
