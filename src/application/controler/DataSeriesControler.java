package application.controler;

import application.Main;
import application.model.Equation;
import application.model.TypeC;
import application.model.TypeFoncPosInit;
import application.model.TypeFoncVitInit;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.paint.Color;

public class DataSeriesControler {

	private Main main;
	private Equation equation;
	private TypeFoncPosInit typeFoncPosInit;
	private TypeFoncVitInit typeFoncVitInit;
	private TypeC typeC;
	private double width, height, max, i, j;
	private GraphicsContext gc;
	
	public DataSeriesControler(Main main, Equation equation) {
		this.main=main;
		this.equation=equation;
		this.typeFoncPosInit=TypeFoncPosInit.CORDE_GUITARE;
		this.typeFoncVitInit=TypeFoncVitInit.CONSTANTE;
		this.typeC=TypeC.AFFINE;
		this.gc=main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getGraphicsContext2D();
	}
	
	//Tous les calculs de données
	public void calculDatas() {
		switch ((int)(equation.getTemps().doubleValue()/equation.getDt().doubleValue())) {
		case 0://Au premier instant
			//Efface les données déjà dans les collections
			equation.getSeries().getData().clear();
			equation.getDatas().clear();
			equation.getPosInits().clear();
			equation.getTempsInits().clear();

			width=main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getWidth()*equation.getDx().doubleValue()/main.getEquation().gethBorder().doubleValue();
			height=main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getHeight()*equation.getDt().doubleValue()/30;
			j=main.getPictogrammeScrollPaneMapping().getPictogrammeCanvas().getHeight()-height;
			max=0;
			switch (typeFoncPosInit) {
			case CORDE_GUITARE:
				if(main.getPersonGridPaneMapping().getNormalToggleButton().isSelected()){
					equation.posInit((x)->main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().getValue()/main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().getValue()*x, 
						(x)->-main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().getValue()*x/(equation.gethBorder().doubleValue()-main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().getValue())+main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurSlider().getValue()*equation.gethBorder().doubleValue()/(equation.gethBorder().doubleValue()-main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().getValue()), main.getCordeGuitarePosInitGridPaneMapping().getFxPositionSlider().getValue());
				}else {
					equation.posInit((x)->Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().getText())/Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().getText())*x, 
							(x)->-Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().getText())*x/(equation.gethBorder().doubleValue()-Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().getText()))+Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxHauteurTextField().getText())*equation.gethBorder().doubleValue()/(equation.gethBorder().doubleValue()-Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().getText())), Double.parseDouble(main.getCordeGuitarePosInitGridPaneMapping().getFxPositionTextField().getText()));
				}
				break;
			case PARABOLE:
				double a,b;
				if(main.getPersonGridPaneMapping().getNormalToggleButton().isSelected()) {
					a=-4*main.getParabolePosInitGridPaneMapping().gethSlider().getValue()/Math.pow(main.getParamFoncGridPaneMapping().getlSlider().getValue(),2);
					b=4*main.getParabolePosInitGridPaneMapping().gethSlider().getValue()/main.getParamFoncGridPaneMapping().getlSlider().getValue();
				}else {
					a=-4*Double.parseDouble(main.getParabolePosInitGridPaneMapping().gethTextField().getText())/Math.pow(main.getEquation().gethBorder().doubleValue(), 2);
					b=4*Double.parseDouble(main.getParabolePosInitGridPaneMapping().gethTextField().getText())/main.getEquation().gethBorder().doubleValue();
				}
				equation.posInit((x)->a*x*x+b*x);
			default:
				break;
			}
			
			switch (typeFoncVitInit) {
			case CONSTANTE:
				if(main.getPersonGridPaneMapping().getNormalToggleButton().isSelected())
					equation.vitInit((x)->main.getConstanteVitInitGridPaneMapping().getvSlider().getValue());
				else
					equation.vitInit((x)->Double.parseDouble(main.getConstanteVitInitGridPaneMapping().getvTextField().getText()));
				break;
			case UN_POINT:
				if(main.getPersonGridPaneMapping().getNormalToggleButton().isSelected())
					equation.vitInit((x)->main.getPointVitInitGridPaneMapping().getvSlider().getValue(),main.getPointVitInitGridPaneMapping().getpSlider().getValue());
				else 
					equation.vitInit((x)->Double.parseDouble(main.getPointVitInitGridPaneMapping().getvTextField().getText()), Double.parseDouble(main.getPointVitInitGridPaneMapping().getpTextField().getText()));
				break;
			default:
				break;
			}
			
			switch (typeC) {
			case AFFINE:
				if(main.getPersonGridPaneMapping().getNormalToggleButton().isSelected())
					equation.cCar((x)->main.getAffineCCarreeGridPaneMapping().getmSlider().getValue()*x+main.getAffineCCarreeGridPaneMapping().getbSlider().getValue());
				else
					equation.cCar((x)->Double.parseDouble(main.getAffineCCarreeGridPaneMapping().getmTextField().getText())*x+Double.parseDouble(main.getAffineCCarreeGridPaneMapping().getbTextField().getText()));
				break;
			case CONSTANTE:
				if(main.getPersonGridPaneMapping().getNormalToggleButton().isSelected())
					equation.cCar((x)->main.getConstanteCCarreeGridPaneMapping().getcSlider().getValue());
				else
					equation.cCar((x)->Double.parseDouble(main.getConstanteCCarreeGridPaneMapping().getcTextField().getText()));
			default:
				break;
			}

			equation.getTempsInits().add(new Data<Number, Number>(0d, 0d));
			for(double x=equation.getDx().doubleValue();x<equation.gethBorder().doubleValue();x+=equation.getDx().doubleValue()) 
				equation.getTempsInits().add(new Data<Number, Number>(x, equation.getTempsInitFunction().apply(x)));
			equation.getTempsInits().add(new Data<Number, Number>(equation.gethBorder().doubleValue(), 0d));

			equation.getDatasTempsPresent().setAll(equation.getTempsInits());
			equation.getDatasTempsPasse().setAll(equation.getPosInits());
			equation.getSeries().getData().setAll(equation.getPosInits());
			max=equation.getSeries().getData().parallelStream().mapToDouble(d->d.getYValue().doubleValue()).max().orElse(0);
			break;
		case 1:
			equation.getSeries().getData().setAll(equation.getTempsInits());
			break;
		default:
			equation.getDatas().clear();

			equation.getDatas().add(new Data<Number, Number>(0d, 0d));
			for(double x=equation.getDx().doubleValue();x<equation.gethBorder().doubleValue()-equation.getDx().doubleValue();x+=equation.getDx().doubleValue())
				equation.getDatas().add(new Data<Number, Number>(x, equation.getDataFunction().apply(x)));
			equation.getDatas().add(new Data<Number, Number>(equation.gethBorder().doubleValue(), 0d));
			
			equation.getDatasTempsPasse().setAll(equation.getDatasTempsPresent());
			equation.getDatasTempsPresent().setAll(equation.getDatas());
			
			equation.getSeries().getData().setAll(equation.getDatas());
			break;
		}
		if(equation.getTemps().doubleValue()<30) {
			i=0;
			equation.getDatas().stream().mapToDouble(data->data.getYValue().doubleValue()).forEach(y->{
				if(y>=0) {
					if(y/max<=1)
						gc.setFill(new Color(y/max, (1-y/max), 0, 1));
					else if(y/max<=2) 
						gc.setFill(new Color(2-y/max, 0, 0, 1));
					else
						gc.setFill(new Color(0.3, 0, 0, 1));
				}else {
					if(y/max>=-1)
						gc.setFill(new Color(0, 1+y/max, -y/max, 1));
					else if(y/max>=-2)
						gc.setFill(new Color(0, 0, 2+y/max, 1));
					else
						gc.setFill(new Color(0, 0, 0.3, 1));
				}
				gc.fillRect(i, j, width, height);
				i+=width;
			});
		}
		j-=height;
	}

	public TypeFoncPosInit getTypeFoncPosInit() {
		return typeFoncPosInit;
	}

	public void setTypeFoncPosInit(TypeFoncPosInit typeFoncPosInit) {
		this.typeFoncPosInit = typeFoncPosInit;
	}

	public TypeFoncVitInit getTypeFoncVitInit() {
		return typeFoncVitInit;
	}

	public void setTypeFoncVitInit(TypeFoncVitInit typeFoncVitInit) {
		this.typeFoncVitInit = typeFoncVitInit;
	}

	public TypeC getTypeC() {
		return typeC;
	}

	public void setTypeC(TypeC typeC) {
		this.typeC = typeC;
	}
	
	
}
