package application.controler;

import java.io.IOException;

import application.Main;
import application.model.Equation;
import application.view.ZLineChart;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LoadingTask extends Task<Scene>{
	
	private Main main;
	
	public LoadingTask(Main main) {
		this.main=main;
	}

	@Override
	protected Scene call() throws Exception {
		try {
			FXMLLoader loader1=new FXMLLoader(main.getClass().getResource("view/AffineCCarreeGridPaneFXML.fxml"));
			main.setAffineCCarreeGridPaneFXML((GridPane)loader1.load());
			main.setAffineCCarreeGridPaneMapping(loader1.getController());
			FXMLLoader loader2=new FXMLLoader(main.getClass().getResource("view/ButtonGridPaneFXML.fxml"));
			main.setButtonGridPaneFXML((GridPane)loader2.load());
			main.setButtonGridPaneMapping(loader2.getController());
			FXMLLoader loader3=new FXMLLoader(main.getClass().getResource("view/CordeGuitarePosInitGridPaneFXML.fxml"));
			main.setCordeGuitarePosInitGridPaneFXML((GridPane)loader3.load());
			main.setCordeGuitarePosInitGridPaneMapping(loader3.getController());
			FXMLLoader loader4=new FXMLLoader(main.getClass().getResource("view/LayoutChartGridPaneFXML.fxml"));
			main.setLayoutChartGridPaneFXML((GridPane)loader4.load());
			main.setLayoutChartGridPaneMapping(loader4.getController());
			FXMLLoader loader5=new FXMLLoader(main.getClass().getResource("view/ParamFoncGridPaneFXML.fxml"));
			main.setParamFoncGridPaneFXML((GridPane)loader5.load());
			main.setParamFoncGridPaneMapping(loader5.getController());
			FXMLLoader loader6=new FXMLLoader(main.getClass().getResource("view/TempsGridPaneFXML.fxml"));
			main.setTempsGridPaneFXML((GridPane)loader6.load());
			main.setTempsGridPaneMapping(loader6.getController());
			FXMLLoader loader7=new FXMLLoader(main.getClass().getResource("view/ConstanteCCarreeGridPaneFXML.fxml"));
			main.setConstanteCCarreeGridPaneFXML((GridPane)loader7.load());
			main.setConstanteCCarreeGridPaneMapping(loader7.getController());
			FXMLLoader loader8=new FXMLLoader(main.getClass().getResource("view/ParabolePosInitGridPaneFXML.fxml"));
			main.setParabolePosInitGridPaneFXML((GridPane)loader8.load());
			main.setParabolePosInitGridPaneMapping(loader8.getController());
			FXMLLoader loader9=new FXMLLoader(main.getClass().getResource("view/ConstanteVitInitGridPaneFXML.fxml"));
			main.setConstanteVitInitGridPaneFXML((GridPane)loader9.load());
			main.setConstanteVitInitGridPaneMapping(loader9.getController());
			FXMLLoader loader10=new FXMLLoader(main.getClass().getResource("view/PictogrammeScrollPaneFXML.fxml"));
			main.setPictogrammeScrollPaneFXML((ScrollPane)loader10.load());
			main.setPictogrammeScrollPaneMapping(loader10.getController());
			FXMLLoader loader11=new FXMLLoader(main.getClass().getResource("view/PointVitInitGridPaneFXML.fxml"));
			main.setPointVitInitGridPaneFXML(loader11.load());
			main.setPointVitInitGridPaneMapping(loader11.getController());
			FXMLLoader loader12=new FXMLLoader(main.getClass().getResource("view/PersoGridPaneFXML.fxml"));
			main.setPersonGridPaneFXML(loader12.load());
			main.setPersonGridPaneMapping(loader12.getController());
		}catch(IOException e) {
			e.printStackTrace();
		}
		main.setLineChart(new ZLineChart());
		main.setTimeControler(new TimerControler(main));
		main.setEquation(new Equation());
		main.setDataSeriesControler(new DataSeriesControler(main, main.getEquation()));
		main.sethBox(new HBox(8,main.getAffineCCarreeGridPaneFXML(), main.getCordeGuitarePosInitGridPaneFXML(), main.getConstanteVitInitGridPaneFXML(), main.getPersonGridPaneFXML()));
		main.setvBox(new VBox(main.getLayoutChartGridPaneFXML(), main.getParamFoncGridPaneFXML(), main.getTempsGridPaneFXML(), main.getButtonGridPaneFXML()));
		main.setBorderPane(new BorderPane(main.getLineChart(),null, main.getvBox(), main.gethBox(), main.getPictogrammeScrollPaneFXML()));
		main.getPictogrammeScrollPaneMapping().getScrollPane().setVvalue(1000);
		return new Scene(main.getBorderPane());
	}
}
