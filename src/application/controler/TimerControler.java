package application.controler;

import java.util.Timer;
import java.util.TimerTask;

import application.Main;
import javafx.application.Platform;

public class TimerControler {

	private Timer timer;
	private Main main;
	
	public TimerControler(Main main) {
		this.main=main;
	}
	
	public void startTime() {
		timer=new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				main.getEquation().concatTime();
				Platform.runLater(()->{
					main.getDataSeriesControler().calculDatas();
					main.getLineChart().getData().setAll(main.getEquation().getSeries());
				});
			}
		}, 0, 10);
	}
	
	public void stopTime() {
		timer.cancel();
		timer.purge();
	}
}
