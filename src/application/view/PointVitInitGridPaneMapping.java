package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class PointVitInitGridPaneMapping implements Initializable{

	@FXML
	private Label pValue, vValue;	
	@FXML
	private Slider vSlider, pSlider;
	@FXML
	private HBox pointHBox, vitesseHBox;
	private Tooltip vSliderToolTip, pSliderToolTip;
	private TextField vTextField, pTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		vTextField=new TextField();
		vTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		vTextField.setText(String.valueOf(0));
		pTextField=new TextField();
		pTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		pTextField.setText(String.valueOf(0.25));
		vSliderToolTip=new Tooltip("La vitesse initiale � un certain point.");
		vSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		vSlider.setTooltip(vSliderToolTip);
		vTextField.setTooltip(vSliderToolTip);
		pSliderToolTip=new Tooltip("Le point auquel la vitesse initiale est exerc�e.");
		pSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		pSlider.setTooltip(pSliderToolTip);
		pTextField.setTooltip(pSliderToolTip);
	}
	
	public void changePerso() {
		pointHBox.getChildren().set(1, pTextField);
		vitesseHBox.getChildren().set(1, vTextField);
		vSlider.disableProperty().set(true);
		pSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		pointHBox.getChildren().set(1, pValue);
		vitesseHBox.getChildren().set(1, vValue);
		vSlider.disableProperty().set(false);
		pSlider.disableProperty().set(false);
	}
	
	public Label getpValue() {
		return pValue;
	}

	public Label getvValue() {
		return vValue;
	}

	public Slider getvSlider() {
		return vSlider;
	}

	public Slider getpSlider() {
		return pSlider;
	}
	
	public TextField getvTextField() {
		return vTextField;
	}
	
	public TextField getpTextField() {
		return pTextField;
	}
}
