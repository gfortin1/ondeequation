package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class ParabolePosInitGridPaneMapping implements Initializable{
	
	@FXML
	private Label hValue;
	@FXML
	private Slider hSlider;
	@FXML
	private HBox hBox;
	private Tooltip hSliderToolTip;
	private TextField hTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		hTextField=new TextField();
		hTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		hTextField.setText(String.valueOf(0.0025));
		hSliderToolTip=new Tooltip("Hauteur de la parabole.");
		hSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		hSlider.setTooltip(hSliderToolTip);
		hTextField.setTooltip(hSliderToolTip);
	}
	
	public void changePerso() {
		hBox.getChildren().set(1, hTextField);
		hSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		hBox.getChildren().set(1, hValue);
		hSlider.disableProperty().set(false);
	}
	
	public Label gethValue() {
		return hValue;
	}

	public Slider gethSlider() {
		return hSlider;
	}
	
	public TextField gethTextField() {
		return hTextField;
	}
}
