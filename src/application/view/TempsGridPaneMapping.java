package application.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class TempsGridPaneMapping{
	
	@FXML
	private Label tempsValue;

	public Label getTempsValue() {
		return tempsValue;
	}
}
