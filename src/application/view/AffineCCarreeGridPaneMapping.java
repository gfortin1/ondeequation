package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;


public class AffineCCarreeGridPaneMapping implements Initializable{

	@FXML
	private Label mValue, bValue;
	@FXML
	private Slider mSlider, bSlider;
	@FXML
	private HBox hBox;
	private Tooltip mSliderToolTip, bSliderToolTip;
	private TextField mTextField, bTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mTextField=new TextField();
		mTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		mTextField.setText(String.valueOf(0.5));
		bTextField=new TextField();
		bTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		bTextField.setText(String.valueOf(0));
		mSliderToolTip=new Tooltip("La pente de l'�quation affine.");
		mSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		mSlider.setTooltip(mSliderToolTip);
		mTextField.setTooltip(mSliderToolTip);
		bSliderToolTip=new Tooltip("L'ordonn�e � l'origine de l'�quation affine.");
		bSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		bSlider.setTooltip(bSliderToolTip);
		bTextField.setTooltip(bSliderToolTip);	
	}
	
	public void changePerso() {
		hBox.getChildren().set(1, mTextField);
		hBox.getChildren().set(3, bTextField);
		mSlider.disableProperty().set(true);
		bSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		hBox.getChildren().set(1, mValue);
		hBox.getChildren().set(3, bValue);
		mSlider.disableProperty().set(false);
		bSlider.disableProperty().set(false);
	}

	public Label getmValue() {
		return mValue;
	}

	public Label getbValue() {
		return bValue;
	}

	public Slider getmSlider() {
		return mSlider;
	}

	public Slider getbSlider() {
		return bSlider;
	}

	public Tooltip getbSliderToolTip() {
		return bSliderToolTip;
	}
	
	public TextField getmTextField() {
		return mTextField;
	}

	public TextField getbTextField() {
		return bTextField;
	}
}
