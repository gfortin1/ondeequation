package application.view;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;

public class ZLineChart extends LineChart<Number, Number>{
	
	private static NumberAxis xAxis=new NumberAxis(0, 0.5, 0.05), yAxis=new NumberAxis(-0.01, 0.01, 0.002);

	public ZLineChart() {
		super(xAxis, yAxis);
		setAnimated(false);
		xAxis.setAutoRanging(false);
		xAxis.setAnimated(true);
		yAxis.setAutoRanging(false);
		yAxis.setAnimated(true);
	}

	public NumberAxis getxAxis() {
		return xAxis;
	}

	public NumberAxis getyAxis() {
		return yAxis;
	}

}
