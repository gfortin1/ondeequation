package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class CordeGuitarePosInitGridPaneMapping implements Initializable{

	@FXML
	private Label fxHauteurValue, fxPositionValue;
	@FXML
	private Slider fxHauteurSlider, fxPositionSlider;
	@FXML
	private HBox positionHBox, hauteurHBox;
	private Tooltip fxHauteurSliderToolTip, fxPositionSliderToolTip;
	private TextField fxHauteurTextField, fxPositionTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fxHauteurTextField=new TextField();
		fxHauteurTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		fxHauteurTextField.setText(String.valueOf(0.0025));
		fxPositionTextField=new TextField();
		fxPositionTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		fxPositionTextField.setText(String.valueOf(0.25));
		fxHauteurSliderToolTip=new Tooltip("Hauteur de la prise.\n"+
										"La hauteur maximale est de 1%\n"+
										"de la longueur de la corde.");
		fxHauteurSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		fxHauteurSlider.setTooltip(fxHauteurSliderToolTip);
		fxHauteurTextField.setTooltip(fxHauteurSliderToolTip);
		fxPositionSliderToolTip=new Tooltip("La position de la cassure de la corde.");
		fxPositionSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		fxPositionSlider.setTooltip(fxPositionSliderToolTip);
		fxPositionTextField.setTooltip(fxPositionSliderToolTip);
	}
	
	public void changePerso() {
		positionHBox.getChildren().set(1, fxPositionTextField);
		fxPositionSlider.disableProperty().set(true);
		hauteurHBox.getChildren().set(1, fxHauteurTextField);
		fxHauteurSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		positionHBox.getChildren().set(1, fxPositionValue);
		fxPositionSlider.disableProperty().set(false);
		hauteurHBox.getChildren().set(1, fxHauteurValue);
		fxHauteurSlider.disableProperty().set(false);
	}

	public Label getFxHauteurValue() {
		return fxHauteurValue;
	}

	public Label getFxPositionValue() {
		return fxPositionValue;
	}

	public Slider getFxHauteurSlider() {
		return fxHauteurSlider;
	}

	public Slider getFxPositionSlider() {
		return fxPositionSlider;
	}

	public TextField getFxHauteurTextField() {
		return fxHauteurTextField;
	}

	public TextField getFxPositionTextField() {
		return fxPositionTextField;
	}
	
}
