package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import application.model.TypeC;
import application.model.TypeFoncPosInit;
import application.model.TypeFoncVitInit;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class ParamFoncGridPaneMapping implements Initializable{

	@FXML
	private Label lValue;
	@FXML
	private Slider lSlider;
	@FXML
	private ComboBox<TypeFoncPosInit> posInitComboBox;
	@FXML
	private ComboBox<TypeFoncVitInit> vitInitComboBox;
	@FXML
	private ComboBox<TypeC> cComboBox;
	@FXML
	private HBox hBox;
	private Tooltip lSliderToolTip, posInitComboBoxToolTip, vitInitComboBoxToolTip, cComboBoxToolTip;
	private TextField lTextField;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lTextField=new TextField();
		lTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		lTextField.setText(String.valueOf(0.5));
		lSliderToolTip=new Tooltip("Longueur de la corde.");
		lSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		lSlider.setTooltip(lSliderToolTip);
		lTextField.setTooltip(lSliderToolTip);
		posInitComboBoxToolTip=new Tooltip("Diff�rents types d'�quations composants\n"+
											"la position initiale.");
		posInitComboBoxToolTip.setTextAlignment(TextAlignment.CENTER);
		posInitComboBox.setTooltip(posInitComboBoxToolTip);
		posInitComboBox.getItems().addAll(TypeFoncPosInit.values());
		posInitComboBox.getSelectionModel().selectFirst();
		vitInitComboBoxToolTip=new Tooltip("Diff�rents types d'�quations composants\n"+
											"la vitesse initiale.");
		vitInitComboBoxToolTip.setTextAlignment(TextAlignment.CENTER);
		vitInitComboBox.setTooltip(vitInitComboBoxToolTip);
		vitInitComboBox.getItems().addAll(TypeFoncVitInit.values());
		vitInitComboBox.getSelectionModel().selectFirst();
		cComboBoxToolTip=new Tooltip("Diff�rents types d'�quations composants\n"+
										"la valeur de C� en tout point.");
		cComboBoxToolTip.setTextAlignment(TextAlignment.CENTER);
		cComboBox.setTooltip(cComboBoxToolTip);
		cComboBox.getItems().addAll(TypeC.values());
		cComboBox.getSelectionModel().select(1);
	}
	
	public void changePerso() {
		hBox.getChildren().set(1, lTextField);
		lSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		hBox.getChildren().set(1, lValue);
		lSlider.disableProperty().set(false);
	}

	public Label getlValue() {
		return lValue;
	}

	public Slider getlSlider() {
		return lSlider;
	}

	public ComboBox getPosInitComboBox() {
		return posInitComboBox;
	}

	public ComboBox getVitInitComboBox() {
		return vitInitComboBox;
	}

	public ComboBox getcComboBox() {
		return cComboBox;
	}
	
	public TextField getlTextField() {
		return lTextField;
	}
}
