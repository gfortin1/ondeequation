package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;

public class ButtonGridPaneMapping implements Initializable{
	
	@FXML
	private Button demarrerButton, arreterButton, remiseZeroButton;
	private Tooltip demarrerButtonToolTip, arreterButtonToolTip, remiseZeroButtonToolTip;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		arreterButton.disableProperty().set(true);
		remiseZeroButton.disableProperty().set(true);
		demarrerButtonToolTip=new Tooltip("D�marrer la simulation.\n"+
										"Attention! Une fois la simulation d�marr�e,\n"+
										"les composantes ne peuvent plus �tre modifi�es.");
		demarrerButtonToolTip.setTextAlignment(TextAlignment.CENTER);
		demarrerButton.setTooltip(demarrerButtonToolTip);
		arreterButtonToolTip=new Tooltip("Met la simulation en attente.");
		arreterButtonToolTip.setTextAlignment(TextAlignment.CENTER);
		arreterButton.setTooltip(arreterButtonToolTip);
		remiseZeroButtonToolTip=new Tooltip("Tous les composantes sont r�initialis�es par les valeurs par d�faut.");
		remiseZeroButtonToolTip.setTextAlignment(TextAlignment.CENTER);
		remiseZeroButton.setTooltip(remiseZeroButtonToolTip);
	}

	public Button getDemarrerButton() {
		return demarrerButton;
	}

	public Button getArreterButton() {
		return arreterButton;
	}

	public Button getRemiseZeroButton() {
		return remiseZeroButton;
	}
}
