package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class ConstanteCCarreeGridPaneMapping implements Initializable{
	
	@FXML
	private Label cValue;
	@FXML
	private Slider cSlider;
	@FXML
	private HBox hBox;
	private Tooltip cSliderToolTip;
	private TextField cTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		cTextField=new TextField();
		cTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		cTextField.setText(String.valueOf(0.5));
		cSliderToolTip=new Tooltip("Valeur de la constante C�.");
		cSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		cSlider.setTooltip(cSliderToolTip);
		cTextField.setTooltip(cSliderToolTip);
	}
	
	public void changePerso() {
		hBox.getChildren().set(1, cTextField);
		cSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		hBox.getChildren().set(1, cValue);
		cSlider.disableProperty().set(false);
	}
	
	public Label getcValue() {
		return cValue;
	}

	public Slider getcSlider() {
		return cSlider;
	}

	public TextField getcTextField() {
		return cTextField;
	}
}
