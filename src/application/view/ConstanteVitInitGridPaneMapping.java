package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class ConstanteVitInitGridPaneMapping implements Initializable{
	
	@FXML
	private Label vValue;
	@FXML
	private Slider vSlider;
	@FXML
	private HBox hBox;
	private Tooltip vSliderToolTip;
	private TextField vTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		vTextField=new TextField();
		vTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		vTextField.setText(String.valueOf(0));
		vSliderToolTip=new Tooltip("Valeur de la vitesse initiale en tout point.");
		vSliderToolTip.setTextAlignment(TextAlignment.CENTER);
		vSlider.setTooltip(vSliderToolTip);
		vTextField.setTooltip(vSliderToolTip);
	}
	
	public void changePerso() {
		hBox.getChildren().set(1, vTextField);
		vSlider.disableProperty().set(true);
	}
	
	public void changeNormal() {
		hBox.getChildren().set(1, vValue);
		vSlider.disableProperty().set(false);
	}

	public Label getvValue() {
		return vValue;
	}

	public Slider getvSlider() {
		return vSlider;
	}
	
	public TextField getvTextField() {
		return vTextField;
	}
}
