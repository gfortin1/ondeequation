package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;

public class PictogrammeScrollPaneMapping implements Initializable{

	@FXML
	private Canvas pictogrammeCanvas;
	@FXML
	private ScrollPane scrollPane;
	private Tooltip pictogrammeCanvasToolTip;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		pictogrammeCanvasToolTip=new Tooltip("Ce pictogramme est cr�� � chaque instant pour\n"+
											"les 30 premi�res secondes. Plus un point est bleu\n"+
											"la courbe est basse et plus un point est rouge et\n"+
											" la courbe est haute.");
		pictogrammeCanvasToolTip.setTextAlignment(TextAlignment.CENTER);
		scrollPane.setTooltip(pictogrammeCanvasToolTip);
	}
	
	public Canvas getPictogrammeCanvas() {
		return pictogrammeCanvas;
	}

	public ScrollPane getScrollPane() {
		return scrollPane;
	}
}
