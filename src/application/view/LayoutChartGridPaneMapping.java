package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;
import javafx.util.converter.DoubleStringConverter;

public class LayoutChartGridPaneMapping implements Initializable{

	@FXML
	private TextField xLowChartTextField, xHighChartTextField, yLowChartTextField, yHighChartTextField;
	private Tooltip abscisseToolTip, ordonneeToolTip;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		abscisseToolTip=new Tooltip("Borne de l'axe des abscisses.");
		abscisseToolTip.setTextAlignment(TextAlignment.CENTER);
		ordonneeToolTip=new Tooltip("Borne de l'axe des ordonn�es.");
		ordonneeToolTip.setTextAlignment(TextAlignment.CENTER);
		xLowChartTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		xLowChartTextField.setText(String.valueOf(0));
		xLowChartTextField.setTooltip(abscisseToolTip);
		xHighChartTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		xHighChartTextField.setText(String.valueOf(0.5));
		xHighChartTextField.setTooltip(abscisseToolTip);
		yLowChartTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		yLowChartTextField.setText(String.valueOf(-0.1));
		yLowChartTextField.setTooltip(ordonneeToolTip);
		yHighChartTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		yHighChartTextField.setText(String.valueOf(0.1));
		yHighChartTextField.setTooltip(ordonneeToolTip);
	}

	public TextField getxLowChartTextField() {
		return xLowChartTextField;
	}

	public TextField getxHighChartTextField() {
		return xHighChartTextField;
	}

	public TextField getyLowChartTextField() {
		return yLowChartTextField;
	}

	public TextField getyHighChartTextField() {
		return yHighChartTextField;
	}
}
