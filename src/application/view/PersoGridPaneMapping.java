package application.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.util.converter.DoubleStringConverter;

public class PersoGridPaneMapping implements Initializable{

	@FXML
	private Label dxValue, dtValue;
	@FXML
	private ToggleButton normalToggleButton, personnaliseToggleButton;
	@FXML
	private HBox dxHBox, dtHBox;
	private ToggleGroup group;
	private TextField dxTextField, dtTextField;
	private Tooltip dxToolTip, dtToolTip;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		dxToolTip=new Tooltip("Le segment en x entre deux donn�es.");
		dtToolTip=new Tooltip("Le segment de temps entre deux instants.");
		dxTextField=new TextField();
		dxTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		dxTextField.setText(String.valueOf(0.001));
		dxTextField.setTooltip(dxToolTip);
		dtTextField=new TextField();
		dtTextField.setTextFormatter(new TextFormatter<Double>(new DoubleStringConverter()));
		dtTextField.setText(String.valueOf(0.002));
		dtTextField.setTooltip(dtToolTip);
		group=new ToggleGroup();
		group.getToggles().addAll(normalToggleButton, personnaliseToggleButton);
		normalToggleButton.setSelected(true);
	}
	
	public void changePerso() {
		dxHBox.getChildren().set(1, dxTextField);
		dtHBox.getChildren().set(1, dtTextField);
	}
	
	public void changeNormal() {
		dxTextField.setText(String.valueOf(0.001));
		dtTextField.setText(String.valueOf(0.002));
		dxHBox.getChildren().set(1, dxValue);
		dtHBox.getChildren().set(1, dtValue);
	}
	
	public Label getDxValue() {
		return dxValue;
	}
	public Label getDtValue() {
		return dtValue;
	}
	public ToggleButton getNormalToggleButton() {
		return normalToggleButton;
	}
	public ToggleButton getPersonnaliseToggleButton() {
		return personnaliseToggleButton;
	}
	public TextField getDxTextField() {
		return dxTextField;
	}
	public TextField getDtTextField() {
		return dtTextField;
	}
}
