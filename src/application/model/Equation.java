package application.model;

import java.util.function.Function;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;

public class Equation {

	private Series<Number, Number> series;
	private DoubleProperty hBorder=new SimpleDoubleProperty(0.5);
	private DoubleProperty dx=new SimpleDoubleProperty(0.001),  dt=new SimpleDoubleProperty(0.002), temps=new SimpleDoubleProperty(0);
	private ObservableList<Data<Number, Number>> datas, posInits, tempsInits, datasTempsPresent, datasTempsPasse;
	private ObservableList<Double> cValues, vitInitValues;
	private Function<Double,Double> tempsInitFunction, dataFunction;
	
	public Equation() {
		datas=FXCollections.observableArrayList();
		posInits=FXCollections.observableArrayList();
		tempsInits=FXCollections.observableArrayList();
		cValues=FXCollections.observableArrayList();
		vitInitValues=FXCollections.observableArrayList();
		datasTempsPresent=FXCollections.observableArrayList();
		datasTempsPasse=FXCollections.observableArrayList();
		series=new Series<>();
		
		tempsInitFunction=new Function<Double, Double>() {
			
			double s;
			int index;

			@Override
			public Double apply(Double x) {
				index=(int)(x/dx.doubleValue());
				s=cValues.get(index)*dt.doubleValue()/dx.doubleValue();
				return s/2*(posInits.get(index+1).getYValue().doubleValue()+posInits.get(index-1).getYValue().doubleValue())+posInits.get(index).getYValue().doubleValue()*(1-s)-vitInitValues.get(index);
			}
		};
		
		dataFunction=new Function<Double, Double>() {
			
			double s;
			int index;

			@Override
			public Double apply(Double x) {
				index=(int)(x/dx.doubleValue());
				s=cValues.get(index)*dt.doubleValue()/dx.doubleValue();
				return s*(datasTempsPresent.get(index+1).getYValue().doubleValue()+datasTempsPresent.get(index-1).getYValue().doubleValue())+2*datasTempsPresent.get(index).getYValue().doubleValue()*(1-s)-datasTempsPasse.get(index).getYValue().doubleValue();
			}
		};
	}
	
	public void posInit(Function<Double, Double> fonction) {
		posInits.clear();
		posInits.add(new Data<Number, Number>(0d, 0d));
		for(double x=dx.doubleValue();x<hBorder.doubleValue();x+=dx.doubleValue())
			posInits.add(new Data<Number, Number>(x, fonction.apply(x)));
		posInits.add(new Data<Number, Number>(0d, 0d));
	}
	
	public void posInit(Function<Double, Double> fonction1, Function<Double, Double> fonction2, double p) {
		posInits.clear();
		posInits.add(new Data<Number, Number>(0d, 0d));
		for(double x=dx.doubleValue();x<hBorder.doubleValue();x+=dx.doubleValue()) {
			if(x<=p)
				posInits.add(new Data<Number, Number>(x, fonction1.apply(x)));
			else
				posInits.add(new Data<Number, Number>(x, fonction2.apply(x)));
		}
		posInits.add(new Data<Number, Number>(0d, 0d));
	}
	
	public void vitInit(Function<Double, Double> fonction) {
		vitInitValues.clear();
		for(double x=0;x<=hBorder.doubleValue();x+=dx.doubleValue())
			vitInitValues.add(fonction.apply(x));
	}
	
	public void vitInit(Function<Double, Double> fonction , double point) {
		vitInitValues.clear();
		for(double x=0;x<=hBorder.doubleValue();x+=dx.doubleValue())
			vitInitValues.add((x+dx.doubleValue()>point&&x<=point)?fonction.apply(x):0d);
	}
	
	public void cCar(Function<Double, Double> fonction) {
		cValues.clear();
		for(double x=0;x<=hBorder.doubleValue();x+=dx.doubleValue())
			cValues.add(fonction.apply(x));
	}
	
	public void concatTime() {
		temps.set(temps.get()+dt.doubleValue());
	}
	
	public Series<Number, Number> getSeries() {
		return series;
	}

	public DoubleProperty gethBorder() {
		return hBorder;
	}

	public DoubleProperty getDx() {
		return dx;
	}

	public DoubleProperty getDt() {
		return dt;
	}

	public DoubleProperty getTemps() {
		return temps;
	}

	public ObservableList<Data<Number, Number>> getDatas() {
		return datas;
	}

	public ObservableList<Data<Number, Number>> getPosInits() {
		return posInits;
	}

	public ObservableList<Data<Number, Number>> getTempsInits() {
		return tempsInits;
	}

	public ObservableList<Data<Number, Number>> getDatasTempsPresent() {
		return datasTempsPresent;
	}

	public ObservableList<Data<Number, Number>> getDatasTempsPasse() {
		return datasTempsPasse;
	}

	public ObservableList<Double> getcValues() {
		return cValues;
	}

	public ObservableList<Double> getVitInitValues() {
		return vitInitValues;
	}

	public Function<Double, Double> getTempsInitFunction() {
		return tempsInitFunction;
	}

	public Function<Double, Double> getDataFunction() {
		return dataFunction;
	}
}
