package application.model;

public enum TypeFoncPosInit {

	CORDE_GUITARE("Corde de guitare"),
	PARABOLE("Parabole");
	
	private String nom;
	
	private TypeFoncPosInit(String nom) {
		this.nom=nom;
	}
	
	@Override
	public String toString() {
		return nom;
	}
}
