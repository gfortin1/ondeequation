package application.model;

public enum TypeC {

	CONSTANTE("Constante"),
	AFFINE("Affine");
	
	private String nom;
	
	private TypeC(String nom) {
		this.nom=nom;
	}
	
	@Override
	public String toString() {
		return nom;
	}
}
