package application.model;

public enum TypeFoncVitInit {

	CONSTANTE("Constante"),
	UN_POINT("En un point");
	
	private String nom;
	
	private TypeFoncVitInit(String nom) {
		this.nom=nom;
	}
	
	@Override
	public String toString() {
		return nom;
	}
}
