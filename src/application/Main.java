package application;

import application.controler.DataSeriesControler;
import application.controler.EquationTask;
import application.controler.ListenerTask;
import application.controler.LoadingTask;
import application.controler.TimerControler;
import application.model.Equation;
import application.view.AffineCCarreeGridPaneMapping;
import application.view.ButtonGridPaneMapping;
import application.view.ConstanteCCarreeGridPaneMapping;
import application.view.ConstanteVitInitGridPaneMapping;
import application.view.CordeGuitarePosInitGridPaneMapping;
import application.view.LayoutChartGridPaneMapping;
import application.view.ParabolePosInitGridPaneMapping;
import application.view.ParamFoncGridPaneMapping;
import application.view.PersoGridPaneMapping;
import application.view.PictogrammeScrollPaneMapping;
import application.view.PointVitInitGridPaneMapping;
import application.view.TempsGridPaneMapping;
import application.view.ZLineChart;
import javafx.application.Application;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
	
	private Equation equation;
	private ZLineChart lineChart;
	private BorderPane borderPane;
	private HBox hBox;
	private VBox vBox;
	private ScrollPane pictogrammeScrollPaneFXML;
	private GridPane affineCCarreeGridPaneFXML, buttonGridPaneFXML, cordeGuitarePosInitGridPaneFXML, layoutChartGridPaneFXML, paramFoncGridPaneFXML,
	tempsGridPaneFXML, constanteCCarreeGridPaneFXML, parabolePosInitGridPaneFXML, constanteVitInitGridPaneFXML, pointVitInitGridPaneFXML, personGridPaneFXML;
	private AffineCCarreeGridPaneMapping affineCCarreeGridPaneMapping;
	private ButtonGridPaneMapping buttonGridPaneMapping;
	private CordeGuitarePosInitGridPaneMapping cordeGuitarePosInitGridPaneMapping;
	private LayoutChartGridPaneMapping layoutChartGridPaneMapping;
	private ParamFoncGridPaneMapping paramFoncGridPaneMapping;
	private TempsGridPaneMapping tempsGridPaneMapping;
	private ConstanteCCarreeGridPaneMapping constanteCCarreeGridPaneMapping;
	private ParabolePosInitGridPaneMapping parabolePosInitGridPaneMapping;
	private ConstanteVitInitGridPaneMapping constanteVitInitGridPaneMapping;
	private PictogrammeScrollPaneMapping pictogrammeScrollPaneMapping;
	private PointVitInitGridPaneMapping pointVitInitGridPaneMapping;
	private PersoGridPaneMapping personGridPaneMapping;
	private DataSeriesControler dataSeriesControler;
	private TimerControler timeControler;

	@Override
	public void start(Stage primaryStage) {
		try {
			LoadingTask loadingTask=new LoadingTask(this);
			ListenerTask listenerTask=new ListenerTask(this);
			EquationTask equationTask=new EquationTask(this);
			loadingTask.run();
			loadingTask.setOnSucceeded(e->{
				equationTask.run();
				listenerTask.run();
				loadingTask.getValue().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				initCssSheets();
				primaryStage.setScene(loadingTask.getValue());
				primaryStage.setMaximized(true);
				primaryStage.show();
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initCssSheets() {
		affineCCarreeGridPaneFXML.getStyleClass().add("pane");
		constanteVitInitGridPaneFXML.getStyleClass().addAll("pane","grid-pane");
		pictogrammeScrollPaneFXML.getStyleClass().add("scroll-pane");
		parabolePosInitGridPaneFXML.getStyleClass().addAll("pane","grid-pane");
		tempsGridPaneFXML.getStyleClass().add("pane");
		personGridPaneFXML.getStyleClass().addAll("pane","grid-pane");
		buttonGridPaneFXML.getStyleClass().add("grid-pane");
		paramFoncGridPaneFXML.getStyleClass().add("grid-pane");
		hBox.getStyleClass().add("pane");
		personGridPaneFXML.getStyleClass().add("pane");
		borderPane.getStyleClass().add("pane");
	}
	
	public void initView() {
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public void setBorderPane(BorderPane borderPane) {
		this.borderPane=borderPane;
	}
	
	public void setLineChart(ZLineChart lineChart) {
		this.lineChart=lineChart;
	}
	
	public void setEquation(Equation equation) {
		this.equation=equation;
	}

	public void setAffineCCarreeGridPaneFXML(GridPane affineCCarreeGridPaneFXML) {
		this.affineCCarreeGridPaneFXML = affineCCarreeGridPaneFXML;
	}

	public void setButtonGridPaneFXML(GridPane buttonGridPaneFXML) {
		this.buttonGridPaneFXML = buttonGridPaneFXML;
	}

	public void setCordeGuitarePosInitGridPaneFXML(GridPane cordeGuitarePosInitGridPaneFXML) {
		this.cordeGuitarePosInitGridPaneFXML = cordeGuitarePosInitGridPaneFXML;
	}

	public void setLayoutChartGridPaneFXML(GridPane layoutChartGridPaneFXML) {
		this.layoutChartGridPaneFXML = layoutChartGridPaneFXML;
	}

	public void setParamFoncGridPaneFXML(GridPane paramFoncGridPaneFXML) {
		this.paramFoncGridPaneFXML = paramFoncGridPaneFXML;
	}

	public void setAffineCCarreeGridPaneMapping(AffineCCarreeGridPaneMapping affineCCarreeGridPaneMapping) {
		this.affineCCarreeGridPaneMapping = affineCCarreeGridPaneMapping;
	}

	public void setButtonGridPaneMapping(ButtonGridPaneMapping buttonGridPaneMapping) {
		this.buttonGridPaneMapping = buttonGridPaneMapping;
	}

	public void setCordeGuitarePosInitGridPaneMapping(
			CordeGuitarePosInitGridPaneMapping cordeGuitarePosInitGridPaneMapping) {
		this.cordeGuitarePosInitGridPaneMapping = cordeGuitarePosInitGridPaneMapping;
	}

	public void setLayoutChartGridPaneMapping(LayoutChartGridPaneMapping layoutChartGridPaneMapping) {
		this.layoutChartGridPaneMapping = layoutChartGridPaneMapping;
	}

	public void setParamFoncGridPaneMapping(ParamFoncGridPaneMapping paramFoncGridPaneMapping) {
		this.paramFoncGridPaneMapping = paramFoncGridPaneMapping;
	}

	public ZLineChart getLineChart() {
		return lineChart;
	}

	public BorderPane getBorderPane() {
		return borderPane;
	}

	public GridPane getAffineCCarreeGridPaneFXML() {
		return affineCCarreeGridPaneFXML;
	}

	public GridPane getButtonGridPaneFXML() {
		return buttonGridPaneFXML;
	}

	public GridPane getCordeGuitarePosInitGridPaneFXML() {
		return cordeGuitarePosInitGridPaneFXML;
	}

	public GridPane getLayoutChartGridPaneFXML() {
		return layoutChartGridPaneFXML;
	}

	public GridPane getParamFoncGridPaneFXML() {
		return paramFoncGridPaneFXML;
	}

	public AffineCCarreeGridPaneMapping getAffineCCarreeGridPaneMapping() {
		return affineCCarreeGridPaneMapping;
	}

	public ButtonGridPaneMapping getButtonGridPaneMapping() {
		return buttonGridPaneMapping;
	}

	public CordeGuitarePosInitGridPaneMapping getCordeGuitarePosInitGridPaneMapping() {
		return cordeGuitarePosInitGridPaneMapping;
	}

	public LayoutChartGridPaneMapping getLayoutChartGridPaneMapping() {
		return layoutChartGridPaneMapping;
	}

	public ParamFoncGridPaneMapping getParamFoncGridPaneMapping() {
		return paramFoncGridPaneMapping;
	}

	public Equation getEquation() {
		return equation;
	}
	
	public GridPane getTempsGridPaneFXML() {
		return tempsGridPaneFXML;
	}

	public void setTempsGridPaneFXML(GridPane tempsGridPaneFXML) {
		this.tempsGridPaneFXML = tempsGridPaneFXML;
	}

	public TempsGridPaneMapping getTempsGridPaneMapping() {
		return tempsGridPaneMapping;
	}

	public void setTempsGridPaneMapping(TempsGridPaneMapping tempsGridPaneMapping) {
		this.tempsGridPaneMapping = tempsGridPaneMapping;
	}

	public DataSeriesControler getDataSeriesControler() {
		return dataSeriesControler;
	}

	public void setDataSeriesControler(DataSeriesControler dataSeriesControler) {
		this.dataSeriesControler = dataSeriesControler;
	}

	public TimerControler getTimeControler() {
		return timeControler;
	}

	public void setTimeControler(TimerControler timeControler) {
		this.timeControler = timeControler;
	}

	public GridPane getConstanteCCarreeGridPaneFXML() {
		return constanteCCarreeGridPaneFXML;
	}

	public void setConstanteCCarreeGridPaneFXML(GridPane constanteCCarreeGridPaneFXML) {
		this.constanteCCarreeGridPaneFXML = constanteCCarreeGridPaneFXML;
	}

	public ConstanteCCarreeGridPaneMapping getConstanteCCarreeGridPaneMapping() {
		return constanteCCarreeGridPaneMapping;
	}

	public void setConstanteCCarreeGridPaneMapping(ConstanteCCarreeGridPaneMapping constanteCCarreeGridPaneMapping) {
		this.constanteCCarreeGridPaneMapping = constanteCCarreeGridPaneMapping;
	}

	public HBox gethBox() {
		return hBox;
	}

	public void sethBox(HBox hBox) {
		this.hBox = hBox;
	}

	public VBox getvBox() {
		return vBox;
	}

	public void setvBox(VBox vBox) {
		this.vBox = vBox;
	}

	public GridPane getParabolePosInitGridPaneFXML() {
		return parabolePosInitGridPaneFXML;
	}

	public void setParabolePosInitGridPaneFXML(GridPane parabolePosInitGridPaneFXML) {
		this.parabolePosInitGridPaneFXML = parabolePosInitGridPaneFXML;
	}

	public ParabolePosInitGridPaneMapping getParabolePosInitGridPaneMapping() {
		return parabolePosInitGridPaneMapping;
	}

	public void setParabolePosInitGridPaneMapping(ParabolePosInitGridPaneMapping parabolePosInitGridPaneMapping) {
		this.parabolePosInitGridPaneMapping = parabolePosInitGridPaneMapping;
	}

	public GridPane getConstanteVitInitGridPaneFXML() {
		return constanteVitInitGridPaneFXML;
	}

	public void setConstanteVitInitGridPaneFXML(GridPane constanteVitInitGridPaneFXML) {
		this.constanteVitInitGridPaneFXML = constanteVitInitGridPaneFXML;
	}

	public ConstanteVitInitGridPaneMapping getConstanteVitInitGridPaneMapping() {
		return constanteVitInitGridPaneMapping;
	}

	public void setConstanteVitInitGridPaneMapping(ConstanteVitInitGridPaneMapping constanteVitInitGridPaneMapping) {
		this.constanteVitInitGridPaneMapping = constanteVitInitGridPaneMapping;
	}

	public ScrollPane getPictogrammeScrollPaneFXML() {
		return pictogrammeScrollPaneFXML;
	}

	public void setPictogrammeScrollPaneFXML(ScrollPane pictogrammeScrollPaneFXML) {
		this.pictogrammeScrollPaneFXML = pictogrammeScrollPaneFXML;
	}

	public PictogrammeScrollPaneMapping getPictogrammeScrollPaneMapping() {
		return pictogrammeScrollPaneMapping;
	}

	public void setPictogrammeScrollPaneMapping(PictogrammeScrollPaneMapping pictogrammeScrollPaneMapping) {
		this.pictogrammeScrollPaneMapping = pictogrammeScrollPaneMapping;
	}

	public GridPane getPointVitInitGridPaneFXML() {
		return pointVitInitGridPaneFXML;
	}

	public void setPointVitInitGridPaneFXML(GridPane pointVitInitGridPaneFXML) {
		this.pointVitInitGridPaneFXML = pointVitInitGridPaneFXML;
	}

	public PointVitInitGridPaneMapping getPointVitInitGridPaneMapping() {
		return pointVitInitGridPaneMapping;
	}

	public void setPointVitInitGridPaneMapping(PointVitInitGridPaneMapping pointVitInitGridPaneMapping) {
		this.pointVitInitGridPaneMapping = pointVitInitGridPaneMapping;
	}

	public PersoGridPaneMapping getPersonGridPaneMapping() {
		return personGridPaneMapping;
	}

	public void setPersonGridPaneMapping(PersoGridPaneMapping personGridPaneMapping) {
		this.personGridPaneMapping = personGridPaneMapping;
	}

	public GridPane getPersonGridPaneFXML() {
		return personGridPaneFXML;
	}

	public void setPersonGridPaneFXML(GridPane personGridPaneFXML) {
		this.personGridPaneFXML = personGridPaneFXML;
	}
}
